import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { StoreComponent } from './Components/store/store.component';
import { AdminComponent } from './Components/admin/admin.component';
import { StatisticsComponent } from './Components/admin/statistics/statistics.component';
import { UsersComponent } from './Components/admin/users/users.component';
import { UserComponent } from './Components/admin/users/user/user.component';
import { ContactComponent } from './Components/contact/contact.component';
import { CartComponent } from './Components/cart/cart.component';


const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'store', component: StoreComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'cart', component: CartComponent },

  { path: 'admin/statistics', component: StatisticsComponent },
  { path: 'admin/users', component: UsersComponent },
  { path: 'admin/users/user', component: UserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
