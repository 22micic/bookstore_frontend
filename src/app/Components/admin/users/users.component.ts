import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users = [
    {name: "Stefan Micic", created: "10.12.2019", status: "Active", email: "stefan.micicdev@gmail.com"},
    {name: "Stefan Micic", created: "10.12.2019", status: "Active", email: "stefan.micicdev@gmail.com"},
    {name: "Stefan Micic", created: "10.12.2019", status: "Active", email: "stefan.micicdev@gmail.com"},
  ]

  constructor() { }

  ngOnInit() {
  }

}
