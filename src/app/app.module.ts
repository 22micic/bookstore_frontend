import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TopNavbarComponent } from './Components/top-navbar/top-navbar.component';
import { FooterComponent } from './Components/footer/footer.component';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { StoreComponent } from './Components/store/store.component';
import { AdminComponent } from './Components/admin/admin.component';
import { StatisticsComponent } from './Components/admin/statistics/statistics.component';
import { UsersComponent } from './Components/admin/users/users.component';
import { UserComponent } from './Components/admin/users/user/user.component';
import { ContactComponent } from './Components/contact/contact.component';
import { CartComponent } from './Components/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavbarComponent,
    FooterComponent,
    HomePageComponent,
    StoreComponent,
    AdminComponent,
    StatisticsComponent,
    UsersComponent,
    UserComponent,
    ContactComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
